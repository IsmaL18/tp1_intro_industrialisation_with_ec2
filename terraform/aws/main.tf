provider "aws" {
  region = "eu-west-1" 
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key-ec2"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCTT85VzMlTU1Eiy9erJNZ47DlSBZsrEQSYXOkJS94ce6g16sYM+POK7m7qEybr/XdCMomqDmd/e8uP7Lnma7IKpSHbCxU6JM/8viaGyua4UJ21RZnF2u1DMTi03KLhZ109ubfDgf4r170n3ilbvoGHbqwSYEN7u/LDMpmoF3vNtNj4USrTB2rykNAMUb+6v+VDLE3NR0bx/XmV9UDkDPvToTCBhyp8QGzwlL/S/4gs2rMhGuYjq/7yxgm/ZMtxu0aw1GHklSCLCIFuyQz2JTFM/R5VqfZ43QYNqRILJahia5WYon0t/Xt5keDv6K7V2l5yd1Zp52jh0rtjTJd6xjsWPjsmQO1arqqCpUhVwWTQXexgRMp5xcVSwzkI3E3LdrI145DOV0YMw+rxeVsRZ83NMSHjC1X6LjePea1ONKcuyGr92MrXSZYlb+uN4LbuD2aZ2+ACsRy8XiuU4OUZPjwqCtVzdW8sP9Zh1MMWoIje7DadwxJ1Ee35vbc30EW88YU= ismad18@DESKTOP-534UJ8U"
}

resource "aws_vpc" "mainvpc" {
  cidr_block = "10.1.0.0/16"
}

resource "aws_security_group" "default" {
  name        = "my-custom-security-group"
  description = "grpe secu customised"
  vpc_id      = aws_vpc.mainvpc.id

  // Exemple de règle d'entrée pour permettre SSH depuis n'importe où
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  // Nouvelle règle d'ingress pour le port 80 (HTTP)
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  // Exemple de règle de sortie autorisant tout le trafic sortant
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "example" {
  ami           = "ami-01dd271720c1ba44f" # Remplacez par l'ID de l'AMI souhaité
  instance_type = "t2.micro" # Remplacez par le type d'instance souhaité

  tags = {
    Name = "EC2-TP1"
  }
}